#!/bin/bash

echo "[burger-maker] readiness check script: check $environment_url/health is responding..."

for attempt in {1..30}
do
    echo "Testing application readiness ($attempt/40)..."
    if wget --no-check-certificate -T 2 --tries 1 "$environment_url/health"
    then
        echo "[INFO] healthcheck responsed: success"
        exit 0
    fi
    sleep 5
done

echo "[ERROR] max attempts reached: failed"
exit 1
