CREATE TABLE purchases (
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  at            DATETIME     NOT NULL,
  burger_id     VARCHAR(255) NOT NULL,
  burger_recipe VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE ingredients (
  id        VARCHAR(255) NOT NULL,
  name      VARCHAR(255) NOT NULL,
  purchase_id BIGINT,
  PRIMARY KEY (id),
  CONSTRAINT FK_ingredient_2_purchase_id FOREIGN KEY (purchase_id) REFERENCES purchases (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
