package org.tbc.samples.cook.domain;

import java.time.Instant;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.AssociationOverride;
import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Schema(description = "A logged purchase")
@Entity(name = "purchases")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Purchase extends PanacheEntityBase {
    @Schema(description = "Purchase's id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Schema(description = "Purchase's date and time", implementation = String.class, format = "date-time")
    @Column
    @NonNull
    private Instant at;

    @Schema(description = "Purchased burger with legal traceability info (ids)")
    @Embedded
    @AttributeOverride(name = "id", column = @Column(name = "burger_id"))
    @AttributeOverride(name = "recipe", column = @Column(name = "burger_recipe"))
    @AssociationOverride(name = "ingredients", joinColumns = @JoinColumn(name = "purchase_id"))
    @NonNull
    private Burger burger;
}
