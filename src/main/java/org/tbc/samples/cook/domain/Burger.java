package org.tbc.samples.cook.domain;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(description = "A burger with its ingredients")
@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Burger {
    @Schema(description = "Burger's id")
    @Column
    private String id;

    @Schema(description = "The burger's recipe")
    @Column
    private String recipe;

    @Schema(description = "The burger is composed of these ingredients")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "burger_id")
    private List<Ingredient> ingredients;
}
