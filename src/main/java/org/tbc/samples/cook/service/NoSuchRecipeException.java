package org.tbc.samples.cook.service;

import jakarta.ws.rs.NotFoundException;

public class NoSuchRecipeException extends NotFoundException { // NOSONAR
    public NoSuchRecipeException(String message) {
        super(message);
    }
}
