package org.tbc.samples.cook.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.tbc.samples.cook.domain.Burger;
import org.tbc.samples.cook.domain.Ingredient;

import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ChefService {

    private static final String BUN = "bun";
    private static final String TOMATO = "tomato";
    private static final String RED_ONION = "red onion";

    private static Random rand = new Random(System.currentTimeMillis());

    private static final Map<String, List<String>> recipes = Map.of(
        "cheeseburger", Arrays.asList(BUN, RED_ONION, TOMATO, "ketchup", "salad", "cheddar", "steak"),
        "bacon", Arrays.asList(BUN, "bacon", "garlic sauce", "brie", "steak", TOMATO),
        "farmer", Arrays.asList(BUN, RED_ONION, "bbq sauce", "stilton", "chicken", TOMATO),
        "fish", Arrays.asList(BUN, "tartare sauce", "cucumber", "old gouda", "fish fillet"),
        "veggie", Arrays.asList(BUN, "goat cheese", "tomato confit", RED_ONION, "super-secret veggie steak"),
        "frenchie", Arrays.asList("baguette", "butter", "brie", "smoked ham"),
        "unreliable", Arrays.asList(BUN, "ingredient A?", "ingredient B?", "ingredient C?")
    );

    public Iterable<String> recipes() {
        return recipes.keySet().stream().sorted().toList();
    }

    public Iterable<String> ingredients(String recipe) {
        if (!recipes.containsKey(recipe)) {
            throw new NoSuchRecipeException("No recipe '" + recipe + "'");
        }
        return recipes.get(recipe);
    }

    public Burger prepareBurger(String recipe) {
        if ("any".equals(recipe)) {
            List<String> names = new ArrayList<>(recipes.keySet());
            recipe = names.get(rand.nextInt(names.size()));
        }
        if (!recipes.containsKey(recipe)) {
            throw new NoSuchRecipeException("No recipe '" + recipe + "'");
        }
        if ("unreliable".equals(recipe)) {
            int div = 42 / 0;
        }
        if ("frenchie".equals(recipe)) {
            throw new NoBurgerAvailableException("We're currently out of stock with '" + recipe + "' burger");
        }
        return new Burger(
                UUID.randomUUID().toString(),
                recipe,
                recipes.get(recipe).stream().map(this::prepareIngredient).collect(Collectors.toList()));
    }

    public Ingredient prepareIngredient(String ingredient) {
        return new Ingredient(UUID.randomUUID().toString(), ingredient);
    }

}
