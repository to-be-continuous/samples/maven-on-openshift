package org.tbc.samples.cook.service;

import jakarta.ws.rs.ServiceUnavailableException;

public class NoBurgerAvailableException extends ServiceUnavailableException { // NOSONAR
    public NoBurgerAvailableException(String message) {
        super(message);
    }
}
