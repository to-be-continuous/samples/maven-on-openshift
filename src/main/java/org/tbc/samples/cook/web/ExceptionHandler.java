package org.tbc.samples.cook.web;

import java.util.Map;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {
        int status = 500;
        String message = "Internal error occurred";
        String details = exception.getMessage();
        if (exception instanceof WebApplicationException) {
            Response resp = ((WebApplicationException) exception).getResponse();
            status = resp.getStatus();
            message = exception.getMessage();
            details = resp.getStatusInfo().getReasonPhrase();
        }
        return Response.status(status)
                .entity(Map.of("code", status, "type", exception.getClass().getSimpleName(), "message", message,
                        "details", details))
                .build();
    }
}
