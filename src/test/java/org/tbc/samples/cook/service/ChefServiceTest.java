package org.tbc.samples.cook.service;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ChefServiceTest {

    private ChefService chefService;

    @BeforeEach
    public void setUp() {
        chefService = new ChefService();
    }

    @Test
    void testRecipes() {
        Iterable<String> recipes = chefService.recipes();
        assertIterableEquals(Arrays.asList("bacon", "cheeseburger", "farmer", "fish", "frenchie", "unreliable", "veggie"), recipes);
    }

    @Test
    void testCheeseburgerIngredients() {
        Iterable<String> ingredients = chefService.ingredients("cheeseburger");
        assertIterableEquals(Arrays.asList("bun", "red onion", "tomato", "ketchup", "salad", "cheddar", "steak"),
                ingredients);
    }

    @Test
    void testNoSuchRecipeIngredients() {
        assertThrows(NoSuchRecipeException.class, () -> chefService.ingredients("nosuchrecipe"));
    }
}
