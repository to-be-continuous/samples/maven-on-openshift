package org.tbc.samples.cook.web;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class BurgerMakerControllerTest {

    @Test
    void shouldReturnBurger() {

        given()
                .when().delete("/api/burgers/cheeseburger")
                .then()
                .statusCode(200)
                .body(
                        "recipe", is("cheeseburger"),
                        "ingredients.size()", is(7),
                        "ingredients[0].name", is("bun"),
                        "ingredients[1].name", is("red onion"),
                        "ingredients[2].name", is("tomato"),
                        "ingredients[3].name", is("ketchup"),
                        "ingredients[4].name", is("salad"),
                        "ingredients[5].name", is("cheddar"),
                        "ingredients[6].name", is("steak"));
    }
}
