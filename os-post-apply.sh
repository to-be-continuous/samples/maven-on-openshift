#!/bin/bash

set -e

echo "[burger-maker] post apply script: trigger a build"
# trigger build (jar -> Docker): this will trigger a deployment
oc start-build "$environment_name" --from-file=target/burger-maker-on-openshift-1.0.0-SNAPSHOT-runner.jar --wait --follow
