#!/bin/bash

set -e

echo "[burger-maker] cleanup environment - deleting application"
oc delete pod,deployment,service,pvc,configmap,secret,build,buildconfig,imagestream,route,deploymentconfig --selector "app=${environment_name}"
