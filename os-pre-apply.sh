#!/bin/bash

set -e

if [[ "${environment_type}" == "review" ]]
then
  echo "[burger-maker] review app: skip"
  exit 0
fi

echo "[burger-maker] pre apply script: create a MariaDB database if does not exist"

if ! oc get deploymentconfig "${environment_name}-db" > /dev/null 2>&1 ; then
  echo "Database does not exist, create new"
  oc new-app mariadb-persistent \
    --param="DATABASE_SERVICE_NAME=${environment_name}-db" \
    --param="MYSQL_DATABASE=burgermakerdb" \
    --param="VOLUME_CAPACITY=64Mi" \
    --labels="env=${environment_type},app=${environment_name},app.kubernetes.io/component=${environment_name},app.kubernetes.io/instance=${environment_name},app.kubernetes.io/name=${environment_name},app.kubernetes.io/part-of=${environment_name},app.openshift.io/runtime=mariadb"

  oc set resources dc/"${environment_name}-db" --limits=cpu=250m,memory=512Mi --requests=cpu=100m,memory=256Mi || true
fi